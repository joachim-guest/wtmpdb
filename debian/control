Source: wtmpdb
Section: admin
Priority: optional
Maintainer: Sven Joachim <svenjoac@gmx.de>
Rules-Requires-Root: no
Build-Depends:
 debhelper (>= 13.11.6~),
 debhelper-compat (= 13),
 docbook-xsl-ns,
 libaudit-dev [linux-any],
 libpam0g-dev,
 libsqlite3-dev,
 meson (>= 0.61.0),
 pkgconf | pkg-config,
 xsltproc,
Standards-Version: 4.6.2
Homepage: https://github.com/thkukuk/wtmpdb
Vcs-Browser: https://salsa.debian.org/joachim-guest/wtmpdb
Vcs-Git: https://salsa.debian.org/joachim-guest/wtmpdb.git

Package: wtmpdb
Architecture: any
Multi-Arch: foreign
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 libpam-wtmpdb,
 ${shlibs:Depends},
 ${misc:Depends},
Description: Year 2038 safe wtmp implementation
 Wtmpdb is a replacement for the `/var/log/wtmp` file which stores
 login and logout time of users as well as boot and shutdown times of
 the machine.  These data are stored in an SQLite database and use
 64-bit timestamps on all architectures, thus it will keep working
 beyond the year 2038.
 .
 This package contains the commandline utility which displays the
 information, as well as the systemd unit files to record boot and
 shutdown times.

Package: libwtmpdb0
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
Description: Year 2038 safe wtmp implementation - shared library
 Wtmpdb is a replacement for the `/var/log/wtmp` file which stores
 login and logout time of users as well as boot and shutdown times of
 the machine.  These data are stored in an SQLite database and use
 64-bit timestamps on all architectures, thus it will keep working
 beyond the year 2038.
 .
 This package contains the shared library.

Package: libwtmpdb-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libwtmpdb0 (= ${binary:Version}),
 ${misc:Depends},
Description: Year 2038 safe wtmp implementation - development files
 Wtmpdb is a replacement for the `/var/log/wtmp` file which stores
 login and logout time of users as well as boot and shutdown times of
 the machine.  These data are stored in an SQlite database and use
 64-bit timestamps on all architectures, thus it will keep working
 beyond the year 2038.
 .
 This package contains the development files.

Package: libpam-wtmpdb
Architecture: any
Multi-Arch: same
Depends:
 libpam-runtime,
 ${shlibs:Depends},
 ${misc:Depends},
Recommends:
 wtmpdb,
Description: Year 2038 safe wtmp implementation - PAM module
 Wtmpdb is a replacement for the `/var/log/wtmp` file which stores
 login and logout time of users as well as boot and shutdown times of
 the machine.  These data are stored in an SQlite database and use
 64-bit timestamps on all architectures, thus it will keep working
 beyond the year 2038.
 .
 This package contains the PAM module to record login and logout
 times of users.
